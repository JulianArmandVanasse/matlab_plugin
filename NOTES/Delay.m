classdef Delay < audioPlugin                            % <== (1) Inherit from audioPlugin.
    properties
        DelayTime = 1;                                            % <== (2) Define tunable property.
        Feedback = 0.3;
        
        readSample = 1;
        writeSample = 20000;
        delayBuffer = zeros(48000, 2);
    end
    properties (Constant)
        PluginInterface = audioPluginInterface( ...           % <== (3) Map tunable property to plugin parameter.
            audioPluginParameter('DelayTime', ...
                'Mapping',{'pow',2,1,20000}), ...
            audioPluginParameter('Feedback', ...
                'Mapping',{'pow',2,0,1-.01}));
            
    end
    methods
        function out = process(plugin,in)                     %< == (4) Define audio processing.
            
            % size of input
            [numSamples,numChannels] = size(in);
            
            % malloc output 
            out = zeros(numSamples,numChannels);
            
            % update write position from interface
            plugin.writeSample = round(mod(plugin.readSample + plugin.DelayTime, ...
                                    length(plugin.delayBuffer))+1);
                                
            % debugging:      
            % fprintf("%i \n", plugin.writeSample);
            % fprintf("%i, %i \n", numSamples, numChannels);

            for n = 1:numSamples
                
                % assign output
                out(n,:) = plugin.delayBuffer(plugin.readSample);
                
                % write to buffer
                plugin.delayBuffer(plugin.writeSample,:) = in(n,:) + ...
                                        plugin.Feedback * out(n,:);
                
                % update read and write locations
                plugin.writeSample = mod(plugin.writeSample, ...
                                        length(plugin.delayBuffer))+1;
                plugin.readSample = mod(plugin.readSample, ...
                                        length(plugin.delayBuffer))+1;
            end

        end
    end
end